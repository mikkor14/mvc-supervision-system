﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyWebApp.Models;

namespace MyWebApp.Controllers
{
    public class HomeController : Controller
    {

        String currentTime = DateTime.Now.ToShortTimeString();

        public IActionResult Index()
        {

            Supervisor Mark = new Supervisor() { Id = 1, Name = "Mark" };
            Supervisor Johnny = new Supervisor() { Id = 2, Name = "Johnny" };
            Supervisor Lisa = new Supervisor() { Id = 3, Name = "Lisa" };
            Supervisor Josh = new Supervisor() { Name = "Josh" };
            Supervisor Noname = new Supervisor();

            SupervisorDB.AddSupervisor(Mark);
            SupervisorDB.AddSupervisor(Johnny);
            SupervisorDB.AddSupervisor(Lisa);
            SupervisorDB.AddSupervisor(Josh);
            SupervisorDB.AddSupervisor(Noname);

            ViewBag.Time =currentTime;
            return View("Home");
        }
        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorDB.AddSupervisor(supervisor);

                return View("SupervisorAdded", supervisor);
            } else
            {
                return View();
            }
          
        }

        [HttpGet]
        public IActionResult AllSupervisors(Supervisor supervisor)
        {
            return View(SupervisorDB.Supervisors1);
        }


        [HttpGet]
        public IActionResult SupervisorsWithSList ()
        {
            return View(SupervisorDB.return4Supervisors().Where(sup => sup.Name.First() == 'S').ToList());
        }

        public IActionResult Home()
        {
            return View("Home");
        }
    }
}
