﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyWebApp.Models
{
    public class Supervisor
    {
        Regex r = new Regex(@"^[a-zA-Z]$");

        [Required(ErrorMessage = "Please enter a valid ID")]
        public int? Id { get; set; } = 0;
        [Required(ErrorMessage = "Please enter a name")]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Please enter a valid name (only letters)")]
        public String Name { get; set; } = "Noname";
    }
}
