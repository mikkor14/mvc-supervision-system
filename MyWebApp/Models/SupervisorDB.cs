﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWebApp.Models
{
    public static class SupervisorDB
    {
        private static List<Supervisor> Supervisors = new List<Supervisor>();

        public static List<Supervisor> Supervisors1 { get => Supervisors; set => Supervisors = value; }

        public static void AddSupervisor(Supervisor supervisor)
        {
            Supervisors.Add(supervisor);
        }

        public static List<Supervisor> return4Supervisors()
        {
            List<Supervisor> supervisorList = new List<Supervisor>();

            Supervisor Mark = new Supervisor() { Id = 1, Name = "Mark" };
            Supervisor Sarah = new Supervisor() { Id = 2, Name = "Sarah" };
            Supervisor Lisa = new Supervisor() { Id = 3, Name = "Lisa" };
            Supervisor Susan = new Supervisor() { Id = 4, Name = "Susan" };

            supervisorList.Add(Mark);
            supervisorList.Add(Sarah);
            supervisorList.Add(Lisa);
            supervisorList.Add(Susan);

            return supervisorList;
        }
    }
}
